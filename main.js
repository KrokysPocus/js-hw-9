function showList(arr, parent = document.body) {
    const list = document.createElement("ul")
    const flattenArr = arr.flat()
    flattenArr.forEach((el) => {
        const listItem = document.createElement("li");
        listItem.innerText = el;
        list.appendChild(listItem);
    })


    const timer = document.createElement("div")
    timer.style.cssText = `
    display: flex;
    justify-content: center;
    width: 300px;
    font-size: 40px;
    font-weight: 600;
    border-radius: 100%;
    background: yellow;
    color: blue;`
    timer.innerText = "05s left"
    let seconds = 5;
    setInterval(() => {
        seconds--;
        timer.textContent = `0${seconds}s left`;
    }, 1000);
    setTimeout(() => {
        document.body.innerHTML = "";
    }, 5000);

    parent.appendChild(list)
    document.body.appendChild(timer)
}


showList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])
